import * as k8s from '@kubernetes/client-node';
import fetch from 'node-fetch';
import https from 'https';
import http from 'http';

// const namespace = process.env.NAMESPACE;
// const TIMEOUT = process.env.TIMEOUT;

const namespace = 'default';
const TIMEOUT = 5000;
let n = 0
let data = ''

const kc = new k8s.KubeConfig();
kc.loadFromDefault();

const opts = {};
kc.applyToRequest(opts);

async function k8sGet(url, timeout=TIMEOUT) {
    const controller = new AbortController();
    const timer = setTimeout(() => {
        controller.abort();
    }, timeout);

    try {
        const agent = new https.Agent(opts);
        const response = await fetch(url, {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            ...opts,
            agent
        });
        const data = await response.json();
        return data;
    } catch (error) {
        if (error instanceof fetch.AbortError) {
            console.log('request was aborted');
        }
    } finally {
        clearTimeout(timer);
    }

}

async function f() {
    setTimeout(() => {
        f();
    }, TIMEOUT);
    let result = await k8sGet(`${kc.getCurrentCluster().server}/apis/batch/v1/namespaces/${namespace}/jobs`)
    n = 0;
    data = '';
    while (result.items[n]) 
    {
        console.log(`cron_job_status{namespace=${namespace}` + ',name='+ result.items[n].metadata.name + ',type=' + result.items[n].status.conditions[0].type + '}\n');
        data=data + `cron_job_status{namespace=${namespace}` + ',name='+ result.items[n].metadata.name + ',type=' + result.items[n].status.conditions[0].type + '}\n';
        n++;
    }
}
f();

const server = http.createServer();
server.on('request', (request, res) => {
  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.end(data);
});

server.listen(8080);